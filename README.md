Индикатор курса usd по данным api.privatbank.ua
===

### Требования:

+ python ~2.7.6
+ pygtk


### Установка и запуск (вручную):

```
cd /opt
git clone git@bitbucket.org:onel/grivna-indicator.git
cd grivna-indicator
chmod a+x GrivnaIndicator.py
python GrivnaIndicator.py
```


### Автозапуск при загрузке:

```
crontab -e
@reboot /opt/grivna-indicator/GrivnaIndicator.py
```