#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# unity grivna applity 
# N.Bogdan <hcbogdan@gmail.com>
#

import sys
import os
import gtk
import urllib
import urllib2
import appindicator
import imaplib
import re
import json


class GrivnaIndicator:
    # refresh time minutes
    REFRESH_MINUTES = 10

    def __init__(self):
        self.ind = appindicator.Indicator('usd-indicator',
                                           os.getcwd()+'/icon.png',
                                           appindicator.CATEGORY_OTHER)

        self.ind.set_status(appindicator.STATUS_ACTIVE)
        self.menu_setup()
        self.ind.set_menu(self.menu)


    def wget(self, url):
        try:
            req = urllib2.Request(url)
            response = urllib2.urlopen(req)
            return response.read()
        except:
            return ''


    def menu_setup(self):
        self.menu = gtk.Menu()

        self.refresh_item = gtk.MenuItem('Обновить')
        self.refresh_item.connect('activate', self.refresh)
        self.refresh_item.show()
        self.menu.append(self.refresh_item)

        self.quit_item = gtk.MenuItem('Закрыть')
        self.quit_item.connect('activate', self.quit)
        self.quit_item.show()
        self.menu.append(self.quit_item)


    def main(self):        
        self.update_rate()
        gtk.timeout_add(self.REFRESH_MINUTES * 60000, self.update_rate)
        gtk.main()


    def quit(self, widget):
        sys.exit(0)


    def refresh(self, widget):
        self.update_rate()


    def update_rate(self):
        try:
            data = self.wget("https://api.privatbank.ua/p24api/pubinfo?jsonp=&exchange=&coursid=11");
            privat = json.loads(data[1:-1]);
            rate = '%0.2f / %0.2f' % (float(privat[2]['buy']), float(privat[2]['sale']))
            self.ind.set_label(rate)
        except TypeError as e:
            print(e);
        except:
            return self.ind.set_label('недоступно');
        return True


if __name__ == '__main__':
    indicator = GrivnaIndicator()
    indicator.main()
